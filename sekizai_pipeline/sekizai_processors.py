import os
import hashlib
from bs4 import BeautifulSoup
from pipeline.templatetags.compressed import CompressedJSNode, CompressedCSSNode
from pipeline.packager import Packager, Package
from pipeline.utils import guess_type
from django.utils.safestring import mark_safe
from django.contrib.staticfiles.storage import staticfiles_storage
from django.utils.encoding import smart_text
from django.template.loader import render_to_string
from pipeline.storage import PipelineStorage
from pipeline.conf import settings


class DetectChangesMixin(object):
    def has_changed(self):
        static_root = getattr(settings, 'STATIC_ROOT')
        # get path to the aggregate file
        path = os.path.join(static_root, self.config['output_filename'])

        source_filenames = self.config['source_filenames']
        # if there are no source files, dont bother with creating aggregation
        if not source_filenames:
            return False

        # if it doesnt exist, signal that we definitely want to create it
        if not os.path.exists(path):
            return True

        last_change = os.path.getmtime(path)
        for source in source_filenames:
            # if some of the sourcefiles were changed since last aggregation
            # pack it again
            try:
                if last_change < os.path.getmtime(os.path.join(static_root, source)):
                    return True
            except OSError:
                # inline scripts are stored elsewhere
                p = os.path.join(os.path.dirname(__file__), 'static', source)
                if not os.path.isfile(p):
                    # new inline script
                    return True

        # nothing new here
        return False


class MyCompressedCSSNode(CompressedCSSNode, DetectChangesMixin):
    def __init__(self, name, config):
        self.name = name
        # save the config here
        self.config = config

    def package_for(self, package_name, package_type):
        # our config is supplied here
        package = {package_name: self.config}
        packager = Packager(storage=PipelineStorage(), css_packages=package, js_packages={})
        self.packager = packager
        return packager.package_for(package_type, package_name)

    def render_css(self, package, path):
        # (re)create the file only if pipeline is enabled and something changed since last aggregation
        if settings.PIPELINE_ENABLED and self.has_changed():
            self.packager.pack_stylesheets(package)
        template_name = package.template_name or "pipeline/css.html"
        context = package.extra_context
        context.update({
                        'type':guess_type(path, 'text/css'),
                        'url': mark_safe(staticfiles_storage.url(path))
                        })
        return render_to_string(template_name, context)


class MyCompressedJSNode(CompressedJSNode, DetectChangesMixin):
    def __init__(self, name, config):
        self.name = name
        self.config = config

    def package_for(self, package_name, package_type):
        package = {package_name: self.config}
        packager = Packager(storage=PipelineStorage(), css_packages={}, js_packages=package)
        self.packager = packager
        return packager.package_for(package_type, package_name)

    def render_js(self, package, path):
        if settings.PIPELINE_ENABLED and self.has_changed():
            self.packager.pack_javascripts(package)
        template_name = package.template_name or "pipeline/js.html"
        context = package.extra_context
        context.update({
                        'type': guess_type(path, 'text/javascript'),
                        'url': mark_safe(staticfiles_storage.url(path))
                        })
        return render_to_string(template_name, context)


def post_process(data, filetype):
    if not getattr(settings, 'PIPELINE_ENABLED', False):
        return data
    soup = BeautifulSoup(data)
    source_filenames = []
    m = hashlib.md5()
    ret = u''
    d = {'js':{'tag':'script',
               'attr':'src'},
         'css':{'tag':'link',
                'attr':'href'}}

    split_files = getattr(settings, 'SPLIT_FILES', False)

    # parse relevant tags <script> or <link>
    static_root = getattr(settings, 'STATIC_ROOT')
    for s in soup.find_all(d[filetype]['tag']):
        try:
            attr = s[d[filetype]['attr']]
        # if it doesnt have src/href attribute its inline and we use it as-is
        except KeyError:
            h = hashlib.md5()
            h.update(s.text.encode('utf8'))
            name = '0' + h.hexdigest()
            inline_name = 'static/{1}/{0}.{1}'.format(name, filetype)
            p = os.path.join(os.path.dirname(__file__), inline_name)
            if not os.path.isfile(p):
                with open(p, 'w+') as f:
                    f.write(s.text.encode('utf8'))
            fn = '/'.join(p.split('/')[-2:])
        else:
            if attr.find('http') == 0:
                ret += smart_text(s)
                continue
            else:
                fn = '/'.join(attr.split('/')[2:])
        source_filenames.append(fn)
        m.update(fn.encode('utf8'))
        name = '0' + m.hexdigest()
    config = {'source_filenames': source_filenames,
              'output_filename': '{1}/{0}.{1}'.format(name, filetype)}

    node = {'css': MyCompressedCSSNode(filetype, config),
            'js': MyCompressedJSNode(filetype, config)}[filetype]

    package = node.package_for(filetype, filetype)
    return ret + node.render_compressed(package, filetype)


def css_post_processor(context, data, namespace):
    return post_process(data, 'css')


def js_post_processor(context, data, namespace):
    return post_process(data, 'js')

