#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

import sekizai_pipeline

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

version = sekizai_pipeline.__version__

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    print("You probably want to also tag the version now:")
    print("  git tag -a %s -m 'version %s'" % (version, version))
    print("  git push --tags")
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='django-sekizai-pipeline',
    version=version,
    description="""on-demand pipeline aggregation with sekizai postprocessors""",
    long_description=readme + '\n\n' + history,
    author='Adam Pospisil',
    author_email='a.pospisil@madalbalstudio.com',
    url='https://github.com/AdamPospisil/django-sekizai-pipeline',
    packages=[
        'sekizai_pipeline',
    ],
    include_package_data=True,
    install_requires=[
    ],
    license="BSD",
    zip_safe=False,
    keywords='django-sekizai-pipeline',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
    ],
)