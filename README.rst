=============================
django-sekizai-pipeline
=============================

.. image:: https://badge.fury.io/py/django-sekizai-pipeline.png
    :target: https://badge.fury.io/py/django-sekizai-pipeline

.. image:: https://travis-ci.org/AdamPospisil/django-sekizai-pipeline.png?branch=master
    :target: https://travis-ci.org/AdamPospisil/django-sekizai-pipeline

.. image:: https://coveralls.io/repos/AdamPospisil/django-sekizai-pipeline/badge.png?branch=master
    :target: https://coveralls.io/r/AdamPospisil/django-sekizai-pipeline?branch=master

on-demand pipeline aggregation with sekizai postprocessors

Documentation
-------------

Coming soon.

.. The full documentation is at https://django-sekizai-pipeline.readthedocs.org.

Quickstart
----------

Install django-sekizai-pipeline::

    pip install django-sekizai-pipeline

Then use it in a project::

    import django-sekizai-pipeline

Features
--------

* TODO