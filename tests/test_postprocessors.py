from django.test import TestCase
from pipeline.conf import settings
from sekizai_pipeline.sekizai_processors import MyCompressedJSNode, js_post_processor
import os
import time
import hashlib


class Test(TestCase):
    def setUp(self):
        self._debug = settings.DEBUG
        self.static_root = getattr(settings, 'STATIC_ROOT')

    def test_pipeline_enabled(self):
        settings.PIPELINE_ENABLED = True
        data = '<script type="text/javascript" src="/static/foo.js"></script>'
        ret = js_post_processor({}, data, 'js')
        m = hashlib.md5()
        m.update('foo.js'.encode('utf8'))
        path = os.path.join(self.static_root, 'js/{}.js'.format(m.hexdigest()))
        try:
            # check that the aggregated file has been created with the correct name
            self.assertTrue(os.path.isfile(path))
        except AssertionError:
            raise
        finally:
            try:
                os.remove(path)
            except:
                pass
        settings.PIPELINE_ENABLED = False
        ret = js_post_processor({}, data, 'js')
        # should not pack aggregated file since pipeline is disabled
        self.assertFalse(os.path.isfile(path))


    def test_has_changed(self):

        config = {'source_filenames': [],
                  'output_filename': 'bar.js'}
        node = MyCompressedJSNode('js', config)
        package = node.package_for('js', 'js')
        # no source files -> False
        self.assertFalse(node.has_changed())

        config = {'source_filenames': ['foo.js'],
                  'output_filename': 'bar.js'}
        node = MyCompressedJSNode('js', config)
        package = node.package_for('js', 'js')
        # no aggregation yet -> True
        self.assertTrue(node.has_changed())

        open(os.path.join(self.static_root, 'bar.js'), 'a')
        # no newer files -> False
        self.assertFalse(node.has_changed())

        time.sleep(1.1)
        open(os.path.join(self.static_root, 'baz.js'), 'a').close()
        config = {'source_filenames': ['foo.js', 'baz.js'],
                  'output_filename': 'bar.js'}
        node = MyCompressedJSNode('js', config)
        package = node.package_for('js', 'js')
        # one newer file -> True
        self.assertTrue(node.has_changed())

    def tearDown(self):
        try:
            os.remove(os.path.join(self.static_root, 'bar.js'))
        except:
            pass
        try:
            os.remove(os.path.join(self.static_root, 'baz.js'))
        except:
            pass
        settings.DEBUG = self._debug
