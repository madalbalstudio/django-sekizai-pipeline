============
Installation
============

At the command line::

    $ easy_install django-sekizai-pipeline

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-sekizai-pipeline
    $ pip install django-sekizai-pipeline